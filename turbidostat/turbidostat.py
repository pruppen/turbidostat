import random
import matplotlib.pyplot as plt
from math import exp, log
import numpy as np


class Turbidostat:
    def __init__(self, target_od, dilution_volume, volume, initial_od=0.1, subpop_size=0.1, growth_rate=1,
                 subpop_growth_rate=0.2, time_step=10 ** -3, takeover_threshold=0.99, noise_level=0.0025):
        self.target_od = target_od
        self.dilution_volume = dilution_volume
        self.volume = volume
        self.od = initial_od
        self.time = 0
        self.time_step = time_step

        self.dilution_factor = self.volume / (self.dilution_volume + self.volume)

        self.pop_od = initial_od * (1 - subpop_size)
        self.pop_growth_rate = growth_rate

        self.takeover_threshold = takeover_threshold

        self.time_to_takeover = 0
        self.subpop_overtaken = 0
        final_fraction = takeover_threshold
        self.time_to_takeover_calculated = (log(final_fraction / (1 - final_fraction)) - log(
            subpop_size / (1 - subpop_size))) / (subpop_growth_rate - growth_rate)

        self.initial_subpop_size = subpop_size
        self.subpop_size = subpop_size
        self.subpop_od = initial_od * subpop_size
        self.subpop_growth_rate = subpop_growth_rate

        self.dilutions_counter = 0

        self.segment_od_list = []
        self.segment_t_list = []
        self.segment_period = []

        self.fit_t = []
        self.fit_growth = []

        self.time_history = []
        self.od_history = []
        self.pop_od_history = []
        self.pop_od_measured_history = []
        self.subpop_od_history = []
        self.subpop_od_measured_history = []
        self.subpop_size_history = []
        self.gamma = []
        self.fit_history = []
        self.growth_rate_history = []

        self.noise_level = noise_level


    def measure_od(self, noise=False):
        if noise:
            sd = self.noise_level
        else:
            sd = 0
        return self.pop_od + random.gauss(0, sd)  # add some noise to the measurement

    def measure_subpop_od(self, noise=False):
        if noise:
            sd = self.noise_level
        else:
            sd = 0
        return self.subpop_od + random.gauss(0, sd)  # add some noise to the measurement

    def step(self, noise=False):
        self.time += self.time_step

        # decide if noisy value is taken for dilution or not
        pop_od_measured = self.measure_od(noise=noise)
        subpop_od_measured = self.measure_subpop_od(noise=noise)
        total_od = pop_od_measured + subpop_od_measured

        self.segment_od_list.append(pop_od_measured+subpop_od_measured)
        self.segment_t_list.append(self.time)


        # Calculate dilution rate based on total population
        # self.od = self.pop_od + self.subpop_od
        # total_od = self.od
        # if total_od > self.target_od:
        #     dilution_rate = self.dilution_rate + self.subpop_growth_rate * self.subpop_size
        # else:
        #     dilution_rate = 0

        if total_od > self.target_od:
            # print(pop_od_measured)
            self.pop_od = self.pop_od * self.dilution_factor * exp(self.pop_growth_rate * self.time_step)
            self.subpop_od = self.subpop_od * self.dilution_factor * exp(self.subpop_growth_rate * self.time_step)
            self.dilutions_counter += 1

            if self.segment_t_list is not [] and self.time > 0.1:
                # calculate growth rate on segment and reset
                # print(self.segment_t_list)
                ln_od_values = np.log(self.segment_od_list)
                # print(self.segment_od_list)
                # print(ln_od_values)
                slope, intercept = np.polyfit(self.segment_t_list, ln_od_values, 1)

                self.fit_t.append(self.time)
                self.fit_growth.append(slope)
                self.segment_period.append(len(self.segment_t_list)*self.time_step)
                self.segment_t_list = []
                self.segment_od_list = []

        else:
            self.pop_od = self.pop_od * exp(self.pop_growth_rate * self.time_step)
            self.subpop_od = self.subpop_od * exp(self.subpop_growth_rate * self.time_step)

        self.od = self.pop_od + self.subpop_od
        self.subpop_size = self.subpop_od / self.od



        # # Dilute culture and adjust subpopulation size
        # od = self.measure_od()
        # if od > self.target_od:
        #     self.od = od * self.volume / (dilution_rate + self.volume)
        #     subpop_od = self.measure_subpop_od()
        #     self.subpop_od = subpop_od * self.subpop_size * self.subpop_growth_rate / (self.dilution_rate + self.volume)
        # else:
        #     self.od = od * dilution_rate / (dilution_rate + self.volume)
        #     self.subpop_size *= 1 + self.subpop_growth_rate

        if self.subpop_overtaken == 0:
            if self.subpop_size > self.takeover_threshold:
                self.time_to_takeover = self.time
                self.subpop_overtaken = 1

        gamma = self.initial_subpop_size * exp(self.subpop_growth_rate * self.time) / \
                (self.initial_subpop_size * exp(self.subpop_growth_rate * self.time) + exp(
                    self.pop_growth_rate * self.time))

        # Save history
        self.time_history.append(self.time)
        self.od_history.append(self.od)
        self.pop_od_history.append(self.pop_od)
        self.subpop_od_history.append(self.subpop_od)
        self.subpop_size_history.append(self.subpop_size)
        self.pop_od_measured_history.append(pop_od_measured)
        self.subpop_od_measured_history.append(subpop_od_measured)
        self.gamma.append(gamma)
        self.growth_rate_history.append(
            self.pop_growth_rate * (1 - self.subpop_size) + self.subpop_growth_rate * self.subpop_size)

    def run(self, num_steps, noise=False):
        for i in range(int(num_steps / self.time_step)):
            self.step(noise)

    def plot(self):
        time = self.time_history
        fig, ax1 = plt.subplots()
        # ax1.plot(time, self.od_history, 'b-', label='Population OD')
        ax1.set_xlabel('Time')
        ax1.set_ylabel('OD')
        # ax2 = ax1.twinx()
        # ax1.plot(time, self.pop_od_history, 'r-', label='1st Population OD')
        # ax1.plot(time, self.subpop_od_history, 'g--', label='Subpopulation OD')
        ax1.plot(time, self.pop_od_measured_history, 'r-', label='WT population OD')
        ax1.plot(time, self.subpop_od_measured_history, 'b-', label='Mutant populaton OD')
        # ax1.plot(time, self.gamma, 'g-', label='gamma')
        div = [i + j for i, j in zip(self.subpop_od_history, self.pop_od_history)]
        # ax1.plot(time, [i / j for i, j in zip(self.subpop_od_history, div)], 'r--', label='gamma2')
        # ax1.set_ylabel('OD/Size')
        if self.fit_history:
            ax1.plot(time, self.fit_history, 'y--', label='fit')
        # plt.yscale("log")
        plt.grid()
        fig.legend(loc='upper left')
        plt.show()

    def plot_OD_tot(self):
        time = self.time_history
        fig, ax1 = plt.subplots()
        # ax1.plot(time, self.od_history, 'b-', label='Population OD')
        ax1.set_xlabel('Time')
        ax1.set_ylabel('OD')
        # ax2 = ax1.twinx()
        # ax1.plot(time, self.pop_od_history, 'r-', label='1st Population OD')
        # ax1.plot(time, self.subpop_od_history, 'g--', label='Subpopulation OD')
        ax1.plot(time, self.od_history, 'b-')

        # ax1.plot(time, self.gamma, 'g-', label='gamma')
        div = [i + j for i, j in zip(self.subpop_od_history, self.pop_od_history)]
        # ax1.plot(time, [i / j for i, j in zip(self.subpop_od_history, div)], 'r--', label='gamma2')
        # ax1.set_ylabel('OD/Size')
        if self.fit_history:
            ax1.plot(time, self.fit_history, 'y--', label='fit')
        # plt.yscale("log")
        plt.grid()
        fig.legend(loc='upper left')
        plt.show()

    def plot_growth_rate(self):
        time = self.time_history
        fig, ax1 = plt.subplots()
        ax1.plot(time, self.growth_rate_history, 'g-', label='overall growth rate')
        ax1.set_xlabel('Time')
        ax1.set_ylabel('growth rate [/h]')
        fig.legend(loc='upper left')
        # fig.title("")
        plt.show()

    def calculate_growth_rate(self, plot=True):
        # fit growth rate
        ln_od_values = np.log(self.od_history)
        slope, intercept = np.polyfit(self.time_history, ln_od_values, 1)
        self.fit_history = [exp(intercept + slope * i) for i in self.time_history]
        # print(slope)
        return self.fit_history


    def plot_growth_rate_fit(self):
        fig, ax1 = plt.subplots()
        ax1.plot(self.fit_t, self.fit_growth, 'g-', label='growth rate fit')
        ax1.plot(self.time_history, self.growth_rate_history, 'b--', label='growth rate no noise')
        ax1.set_xlabel('Time')
        ax1.set_ylabel('growth rate [/h]')
        fig.legend(loc='upper left')
        x = [1.73E+01, 1.80E+01, 1.84E+01, 1.89E+01, 1.94E+01, 1.98E+01, 1.99E+01, 2.02E+01]
        y  = [1.58E-01, 2.11E-01, 2.54E-01, 2.45E-01, 3.06E-01, 3.18E-01, 4.06E-01, 3.61E-01]
        # fig.title("")
        ax1.plot([i-17.3 for i in x], y, 'go')
        plt.show()

    def plot_period(self):
        fig, ax1 = plt.subplots()
        ax1.plot(self.fit_t, self.segment_period, 'g-', label='growth rate fit')
        ax1.set_xlabel('Time')
        ax1.set_ylabel('period duration [h]')
        fig.legend(loc='upper left')
        # fig.title("")
        plt.show()

    def safe_data_to_file(self, data, filename='data.csv', header='', delimiter=','):
        # save pressure and flow data to file
        np.savetxt(filename, data, header=header, delimiter=delimiter)

    def plot_fraction_subpop(self):
        time = self.time_history
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('Time')
        ax1.set_ylabel('OD')

        ax1.plot(time, self.subpop_size_history, 'b-')

        x = [0.75, 1.75, 2.75, 3.75, 4.75, 18.75]
        y = [0.07, 0.09, 0.13, 0.19, 0.5, 0.95]
        ax1.plot(x, y, 'go')
        plt.grid()
        fig.legend(loc='upper left')
        plt.show()

if __name__ == "__main__":

    # Example usage
    t = Turbidostat(target_od=1, dilution_volume=2, volume=20, initial_od=0.1, subpop_size=0.05, growth_rate=0.2,
                    subpop_growth_rate=1.1, time_step=1/60, takeover_threshold=0.9)

    t.run(10, noise=False)
    # t.calculate_growth_rate()
    print("final subpopulation size: {}".format(t.subpop_size))
    print("subpopulation overtaken after: {} h".format(t.time_to_takeover))
    print("calculated subpopulation overtake time: {} h".format(t.time_to_takeover_calculated))
    print("{} times diluted".format(t.dilutions_counter))

    t.plot()
    t.plot_growth_rate()
    t.plot_growth_rate_fit()
    t.plot_period()
    t.plot_OD_tot()
    t.plot_fraction_subpop()


