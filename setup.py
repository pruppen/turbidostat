#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='turbidostat',
    version='0.1.0',
    description='Python library to simulate a turbidostat with dilution happening in pulses.',
    url='https://git.bsse.ethz.ch/pruppen/turbidostat',
    author='Peter Ruppen',
    author_email='peter.ruppen@bsse.ethz.ch',
    license='GNU 3.0',
    packages=find_packages(),
    install_requires=['matplotlib', 'numpy'],

)
